# DiscoTex - Mathify your Discord!

<img src="demo.gif" alt="GIF of DiscoTex in action" width="500"/>

DiscoTex is a Discord bot that provides a `/` command in Discord for automatically rendering TeX expressions. The bot currently provides one command, `/tex`. As an argument, provide any valid math mode expression (something that can go within `$ ... $`). DiscoTex will render your expression using `matplotlib` and send it back as an image.

## Installation and Setup
These installation instructions are for Debian/Ubuntu, although they should work with little adjustment in other Linux distributions. It is recommended to run the bot on a dedicated server, rather than a personal computer.

**Set up the application on Discord:**

In the [Discord Developer Portal](https://discord.com/developers/applications), create a new application. Under the Bots tab, create a new Bot. Then, under the OAuth2 tab, select the URL Generator. On this page, select the scope `bot`, then select the permissions `Send Messages` and `Attach Files`. Finally, paste the generated URL into your browser, and add the bot to whichever servers you want.

**Install git, python, and TeX dependencies:**
```sh
sudo apt update && sudo apt install git python3 python3-pip texlive-base dvipng texlive-latex-extra texlive-fonts-recommended cm-super
```

**Download DiscoTex:**
```sh
git clone https://gitlab.com/jbitz/discotex.git
```
**Create a virtual environment and install python dependencies:**

```sh
pip3 install virtualenv
python3 -m virtualenv env
source env/bin/activate
pip3 install -r requirements.txt
```
**Create a secrets file:**
Replace `YOUR_KEY` with the private token for your bot (found under the Bot tab of the Discord Developer Portal).
```sh
touch secrets.sh && echo export DISCORD_TOKEN=YOUR_KEY > secrets.sh
source secrets.sh
```

**Run the bot:**
```sh
python3 discotex.py &
```
If you are using `ssh` to run the bot on a remote server, it is recommended to use
```sh
nohup python3 discotex.py &
```
to allow the bot to continue running after you log out.

