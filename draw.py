import matplotlib
import matplotlib.pyplot as plt
import io

def render_latex(latex):
    left = -1
    font_size = 60
    while left < 0: # If the text is too large, try again, but smaller font
        plt.cla()
        plt.clf()
        plt.axis('off')
        plt.grid(False)
        fig = matplotlib.pyplot.gcf()
        fig.set_size_inches(7.5, 2.5) # 2:1 aspect ratio
        text = plt.text(.5, .5, latex,
            fontsize=font_size, 
            horizontalalignment='center', 
            verticalalignment='center');
        left = text.get_window_extent().x0
        font_size -= 5
    # Now we can finally save the image into a buffer to send to Discord
    image_buffer = io.BytesIO()
    plt.savefig(image_buffer, format='png')
    image_buffer.seek(0)
    return image_buffer