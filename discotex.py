import os
import discord
import matplotlib
import datetime
import draw
import sys

intents = discord.Intents.default()
client = discord.Client(intents=intents)
tree = discord.app_commands.CommandTree(client)

LOGFILE = 'log.txt'

def log(message):
    print(message)
    logfile = open(LOGFILE, 'a')
    logfile.write(message + '\n')
    logfile.close()

def generate_log_message(interaction, latex, success):
    status = '<success>' if success else '<failed>'
    time = str(datetime.datetime.now())
    message = f'{time}: {status} {interaction.user.name}#{interaction.user.discriminator} {latex}'
    return message

@client.event
async def on_ready():
    matplotlib.rcParams['text.usetex'] = True
    await tree.sync()
    print("ready!")
    
@tree.command(name='tex', description='Automatically render TeX expressions!')
async def render_tex(interaction, latex:str):
    try:
        image_buffer = draw.render_latex(f'$\displaystyle {latex}$')
    except: # Something went wrong with matplotlib
        await interaction.response.send_message(f'Invalid expression, BAKA!\n{latex}')
        log(generate_log_message(interaction, latex, False))
        return

    # Filename must end in '.png', or else Discord won't auto-display it
    response_file = discord.File(image_buffer, filename='tex.png')
    await interaction.response.send_message("", file=response_file)
    log(generate_log_message(interaction, latex, True))

# Application secret token should be stored in an environment variable
client.run(os.environ.get('DISCORD_TOKEN'))


        

